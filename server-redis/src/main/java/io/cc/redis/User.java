package io.cc.redis;

import io.cc.channel.Channel;
import io.cc.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

/**
 * Created by gdiep on 2016-12-06.
 */
public class User implements io.cc.user.User {
    final static Logger logger = LoggerFactory.getLogger(User.class);

    String name;
    Collection<Channel> channels = new HashSet<>();

    public User (String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Collection<Channel> channels() {
        return Collections.unmodifiableCollection(channels);
    }

    @Override
    public void join(Channel channel) {
        channels.add(channel);
        logger.info("User name=" + this.name() + " joined channel name=" + channel.name());
    }

    @Override
    public void request(Channel channel) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void leave(Channel channel) {
        channels.remove(channel);
        logger.info("User name=" + this.name() + " leaved channel name=" + channel.name());
    }

    @Override
    public void send(Message message, Channel channel) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
