package io.cc.redis;

import io.cc.user.*;
import io.cc.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

/**
 * Created by gdiep on 2016-12-06.
 */
public class Channel implements io.cc.channel.Channel {

    final static Logger logger = LoggerFactory.getLogger(Channel.class);

    String name;
    Collection<User> users = new HashSet<>();

    public Channel (String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Collection<User> users() {
        return Collections.unmodifiableCollection(users);
    }

    @Override
    public boolean isPrivate() {
        return false;
    }

    @Override
    public void addUser(User user) {
        users.add(user);
        logger.info("User name=" + user.name() + " added to channel name=" + this.name());
    }

    @Override
    public void removeUser(User user) {
        users.remove(user);
        logger.info("User name=" + user.name() + " removed from channel name=" + this.name());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel channel = (Channel) o;
        return Objects.equals(name, channel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
