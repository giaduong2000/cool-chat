package io.cc.redis;

import io.cc.lambda.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.NoSuchElementException;

import static io.cc.lambda.Utils.of;

/**
 * Created by gdiep on 2016-12-06.
 */
public class Server implements io.cc.server.Server {

    final static Logger logger = LoggerFactory.getLogger(Server.class);

    Collection<Channel> channels = new HashSet<>();

    @Override
    public io.cc.user.User createUser(String name) {
        logger.info("Creating user name=" + name);
        return new User(name);
    }

    @Override
    public Collection<io.cc.channel.Channel> channels() {
        return Collections.unmodifiableCollection(channels);
    }

    @Override
    public io.cc.channel.Channel createChannel(String name) {
        return of(new Channel(name)).with(c -> {
            channels.add(c);
            logger.info("channel name=" + name + " added to server");
        }).get();
    }

    @Override
    public void deleteChannel(String name) {
        of(findChannel(name)).with(channel -> {
            channels.remove(channel);
            logger.info("channel name=" + name + " removed from server");
        });
    }
    private io.cc.channel.Channel findChannel(String name) {
        return channels().stream().filter(channel -> channel.name().equals(name)).findAny()
                .orElseThrow(() -> new NoSuchElementException("Channel " + name + " not found"));
    }

}
