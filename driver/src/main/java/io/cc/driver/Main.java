package io.cc.driver;

import io.cc.channel.Channel;
import io.cc.server.Server;
import io.cc.user.User;

/**
 * Created by gdiep on 2016-12-05.
 */
public class Main {
    public static void main(String[] arg) {
        Server ccServer = new io.cc.redis.Server();
        Channel ccChannel1 = ccServer.createChannel("Channel 1");
        Channel ccChannel2 = ccServer.createChannel("Channel 2");
        User ccUser1 = ccServer.createUser("User 1");
        ccUser1.join(ccChannel1);
    }
}
