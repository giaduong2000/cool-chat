package io.cc.server;

import io.cc.channel.Channel;
import io.cc.user.User;

import java.util.Collection;

/**
 * Created by gdiep on 2016-12-06.
 */
public interface Server {
    User createUser(String name);
    Collection<Channel> channels();
    Channel createChannel(String name);
    void deleteChannel(String name);
}
