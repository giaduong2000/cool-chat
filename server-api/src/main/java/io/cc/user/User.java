package io.cc.user;

import io.cc.channel.Channel;
import io.cc.message.Message;

import java.util.Collection;

/**
 * Created by gdiep on 2016-12-06.
 */
public interface User {
    String name();
    Collection<Channel> channels();
    void join(Channel channel);
    void request(Channel channel);
    void leave(Channel channel);
    void send(Message message, Channel channel);

}
