package io.cc.channel;

import io.cc.user.User;

import java.util.Collection;

/**
 * Created by gdiep on 2016-12-06.
 */
public interface Channel {
    String name();
    Collection<User> users();
    boolean isPrivate();
    void addUser(User user);
    void removeUser(User user);
}
