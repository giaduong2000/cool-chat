package io.cc.message;

/**
 * Created by gdiep on 2016-12-06.
 */
public interface Message {
    byte[] content();
}
