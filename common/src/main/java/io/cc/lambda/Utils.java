package io.cc.lambda;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by gdiep on 2016-12-06.
 */
public final class Utils<T> {
    T o;

    private Utils(T o) {
        this.o = o;
    }
    public T get() {
        return o;
    }

    public static <T> Utils<T> of(T o) {
        return new Utils(o);
    }

    public Utils<T> with(Consumer<T> c) {
        c.accept(o);
        return this;
    }
    public <R> Utils<R> map(Function<T, R> f) {
        return of(f.apply(o));
    }

}
